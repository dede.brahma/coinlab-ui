/* ==========================================================================
    Template by   = Dede Brama Aryanto
    Tools         = Bootstrap 5
    (c)Copyright  = 2021
   ==========================================================================
*/

// Function show login / register
function goRegister(id1, id2, id3, id4) {
    var no_account = document.getElementById(id1);
    var have_account = document.getElementById(id2);
    var form_login = document.getElementById(id3);
    var form_register = document.getElementById(id4);

    no_account.style.display = "none";
    have_account.style.display = "block";
    form_login.style.display = "none";
    form_register.style.display = "block";
}

function goLogin(id1, id2, id3, id4) {
    var no_account = document.getElementById(id1);
    var have_account = document.getElementById(id2);
    var form_login = document.getElementById(id3);
    var form_register = document.getElementById(id4);

    no_account.style.display = "block";
    have_account.style.display = "none";
    form_login.style.display = "block";
    form_register.style.display = "none";
}


// Function profile page
function profileShow(id1, id2, id3) {
    var detail = document.getElementById(id1);
    var update = document.getElementById(id2);
    var valid = document.getElementById(id3);

    detail.style.display = "none";
    update.style.display = "flex";
    valid.style.display = "block";
}

function updateHide(id1, id2, id3) {
    var detail = document.getElementById(id1);
    var update = document.getElementById(id2);
    var valid = document.getElementById(id3);

    detail.style.display = "flex";
    update.style.display = "none";
    valid.style.display = "none";
}

function referalShow(id1, id2, id3) {
    var detail = document.getElementById(id1);
    var update = document.getElementById(id2);
    var valid = document.getElementById(id3);

    detail.style.display = "none";
    update.style.display = "flex";
    valid.style.display = "block";
}

function referalHide(id1, id2, id3) {
    var detail = document.getElementById(id1);
    var update = document.getElementById(id2);
    var valid = document.getElementById(id3);

    detail.style.display = "flex";
    update.style.display = "none";
    valid.style.display = "none";
}

// Function market page
function showAdd(id1, id2, id3) {
    var market = document.getElementById(id1);
    var add = document.getElementById(id2);
    var transfer = document.getElementById(id3);

    market.style.display = "none";
    add.style.display = "block";
    transfer.style.display = "none";
}

function addHide(id1, id2, id3) {
    var market = document.getElementById(id1);
    var add = document.getElementById(id2);
    var transfer = document.getElementById(id3);

    market.style.display = "block";
    add.style.display = "none";
    transfer.style.display = "none";
}

function nextTransfer(id1, id2, id3) {
    var add = document.getElementById(id1);
    var transfer = document.getElementById(id2);
    var market = document.getElementById(id3);

    add.style.display = "none";
    transfer.style.display = "block";
    market.style.display = "none";    
}

function transferHide(id1, id2, id3) {
    var add = document.getElementById(id1);
    var transfer = document.getElementById(id2);
    var market = document.getElementById(id3);

    add.style.display = "block";
    transfer.style.display = "none";
    market.style.display = "none";    
}


// Function exchange page
function showApi(id1, id2) {
    var exchange = document.getElementById(id1);
    var api = document.getElementById(id2);

    exchange.style.display = "none";
    api.style.display = "block";
}

function apiHide(id1, id2) {
    var exchange = document.getElementById(id1);
    var api = document.getElementById(id2);

    exchange.style.display = "block";
    api.style.display = "none";
}