document.addEventListener('DOMContentLoaded', function () {
  const chart = Highcharts.chart('chartMarket', {
    chart: {
      type: 'area',
      backgroundColor: 'transparent'
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'datetime',
      dateTimeLabelFormats: { // don't display the dummy year
          month: '%e. %b',
          year: '%b'
      },
      title: {
        text: ''
      }
    },
    yAxis: {
      labels: {
          format: '{value}%'
      },
      title: {
          enabled: false
      },
      opposite: true
    },
    tooltip: {
      pointFormat: 'Bitcoin'
    },
    plotOptions: {
      series: {
        lineColor: '#00C853',
        marker: {
          enabled: false
          // fillColor: '#FFAB00'
        },
        fillColor: {
          linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
          stops: [
              [0, '#00C853'],
              [1, 'transparent']
          ]
        }
      }
    },
    credits: {
      enabled: false
    },
    series: [{
      name: '',
      color: 'transparent',
      data: [
        [Date.UTC(1970, 10, 25), 0],
        [Date.UTC(1970, 11,  6), 0.25],
        [Date.UTC(1970, 11, 20), 1.41],
        [Date.UTC(1970, 11, 25), 1.64],
        [Date.UTC(1971, 0,  4), 1.6],
        [Date.UTC(1971, 0, 17), 2.55],
        [Date.UTC(1971, 0, 24), 2.62],
        [Date.UTC(1971, 1,  4), 2.5],
        [Date.UTC(1971, 1, 14), 2.42],
        [Date.UTC(1971, 2,  6), 2.74],
        [Date.UTC(1971, 2, 14), 2.62],
        [Date.UTC(1971, 2, 24), 2.6],
        [Date.UTC(1971, 3,  1), 2.81],
        [Date.UTC(1971, 3, 11), 2.63],
        [Date.UTC(1971, 3, 27), 2.77],
        [Date.UTC(1971, 4,  4), 2.68],
        [Date.UTC(1971, 4,  9), 2.56],
        [Date.UTC(1971, 4, 14), 2.39],
        [Date.UTC(1971, 4, 19), 2.3],
        [Date.UTC(1971, 5,  4), 2],
        [Date.UTC(1971, 5,  9), 1.85],
        [Date.UTC(1971, 5, 14), 1.49],
        [Date.UTC(1971, 5, 19), 1.27],
        [Date.UTC(1971, 5, 24), 0.99],
        [Date.UTC(1971, 5, 29), 0.67],
        [Date.UTC(1971, 6,  3), 0.18],
        [Date.UTC(1971, 6,  4), 0]
      ]
    }]
  })
});
